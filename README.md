# huffenc

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

Huffman entropy coder.


## Download

- [⬇️ huffenc-v1.7z](dist-archive/huffenc-v1.7z) *(9.65 KiB)*
